from excel_conv import excel_conv
from xml_conv import xml_conv

if __name__ == "__main__":
    filename = "test.xlsx"
    save_name = "saverke.xlsx"
    min_row = 0
    max_row = 20
    min_col = 0
    max_col = 10

    conv = excel_conv(filename)

    conv.write_cell(1, 1)
    cellVal = conv.read_sheet(min_row, max_row, min_col, max_col)
    print(cellVal)

    conv.save_sheet(save_name)

    # XML stuff
    filename_xml = 'data.xml'
    xml_conv = xml_conv(filename_xml)

    cellback = xml_conv.get_all_atrib()
    print(cellback)

    allvalue = xml_conv.get_all_atrib_data()
    print(allvalue)

    #print(xml_conv.get_atrib('null', "een"))
    print(xml_conv.get_atrib(0, 1))

    print(xml_conv.get_atrib_data(0, 1))

    xml_conv.set_atrib_root("dataaa")
