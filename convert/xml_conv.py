import xml.etree.ElementTree as ET

class xml_conv:
    def __init__(self, filename):
        self.filename = filename

        namelist = self.filename.split(".")
        if (namelist[1] != "xml"):
            txt = "The required extension is {0}, your input was {1}"
            print(txt.format("xml", namelist[1]))
            print("Please give the correct extension")
            raise NameError('wrong name')

        self.tree = ET.parse(filename)
        self.root = self.tree.getroot()

    def get_all_atrib(self):
        cellback = {}
        i=0
        for elem in self.root:
            for subelem in elem:
                cellback[i] = subelem.attrib
                i=i+1

        return cellback

    def get_all_atrib_data(self):
        cellback = {}
        i=0
        for elem in self.root:
            for subelem in elem:
                cellback[i] = subelem.text
                i=i+1

        return cellback

    def get_atrib(self, x, y):
        try:
            val = int(x)
            val = int(y)
            print("Yes input string is an Integer.")
        except ValueError:
            raise ValueError("Wrong input type, both should be int")

        if x<0 or y<0:
            raise ValueError('x and/or y is smaller than zero')

        atrib = self.root[x][y].attrib
        return atrib

    def get_atrib_data(self, x, y):
        try:
            val = int(x)
            val = int(y)
            print("Yes input string is an Integer.")
        except ValueError:
            raise ValueError("Wrong input type, both should be int")

        if x<0 or y<0:
            raise ValueError('x and/or y is smaller than zero')

        atrib = self.root[x][y].text
        return atrib

    def set_atrib_data(self, parent, child, name):
        atrib = ET.SubElement(parent, child)
        atrib.set('name', name)

    def set_atrib_root(self, name):
        data = ET.Element('datas')
        items = ET.SubElement(data, 'items')
        item1 = ET.SubElement(items, 'item1')
        item2 = ET.SubElement(items, 'item2')
        item1.text = 'the text in item1'
        item2.text = 'item2s text'

        mydata = ET.tostring(data)
        myfile = open("xmlwrite.xml", "w")
        myfile.write(str(mydata))

