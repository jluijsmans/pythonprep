from openpyxl import load_workbook

class excel_conv:
    def __init__(self, workbook_name):
        self.workbook_name = workbook_name

        namelist = self.workbook_name.split(".")
        if (namelist[1] != "xlsx"):
            txt = "The required extension is {0}, your input was {1}"
            print(txt.format("xlsx", namelist[1]))
            print("Please give the correct extension")
            raise NameError('wrong name')

        self.workbook = load_workbook(filename=self.workbook_name)
        self.sheet = self.workbook.active

    # Read the values of the cells in the given range
    # Returns a list with the values of the cells
    def read_sheet(self, min_row_set, max_row_set, min_col_set, max_col_set):
        cellVal = {}
        i=0

        if (min_row_set > max_row_set or min_col_set > max_col_set):
                raise ValueError('Invalid min-max input in: read_sheet')

        if (min_row_set < 0 or max_row_set < 0 or min_col_set < 0 or max_col_set < 0):
                raise ValueError('One or more inputs are smaller than zero')

        for value in self.sheet.iter_rows(min_row=min_row_set, max_row=max_row_set, min_col=min_col_set, max_col=max_col_set, values_only=True):
            for col in range(0, len(value)):
                cellVal[i] = value[col]
                i=i+1

        return cellVal

    def write_cell(self, row_set, column_set):
        self.sheet.cell(row=row_set, column=column_set).value = "the written cell"

    def insert_columns(self, colnumber_set, amount_set):
        self.sheet.insert_cols(idx=colnumber_set, amount = amount_set)

    def delete_columns(self, colnumber_set, amount_set):
        self.sheet.delete_cols(idx=colnumber_set, amount = amount_set)

    def insert_rows(self, rownumber_set, amount_set):
        self.sheet.insert_cols(idx=rownumber_set, amount = amount_set)

    def delete_rows(self, rownumber_set, amount_set):
        self.sheet.delete_cols(idx=rownumber_set, amount = amount_set)

    def save_sheet(self, name):
        self.workbook.save(filename=name)


