# VIP preparation for converting tool project
## Progress
### 21/11/2019 + 22/11/2019
Practiced creating a gui in python with the help of the book *pythoni gui programming cookbook*. The progress can be found in the folder *gui*.

### 25/11/2019
Practiced with reading/writing xml and dbc files + work on the poster for the NIT conferences.

## Run the code
### to run the gui code:
* make sure to be in the gui folder
* *python3 main.py*

### To run the convert code of xml and xlsx
* make sure to be in the convert folder
* *python3 main.py*

### To run the dbc code
* make sure to be in the dbc folder
* *python3 dbcConv.py*
