import InterfaceClass as Interface
from threading import Thread

# Start the gui
gui = Interface.Interface()
run_thread = Thread(target=gui.method_in_a_thread)

gui.win.mainloop()
