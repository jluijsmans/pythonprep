import cantools
from pprint import pprint

db = cantools.database.load_file('example.dbc')
print(db.version)

example_message = db.get_message_by_name('ExampleMessage')
pprint(example_message.signals)

with open('example.dbc') as fin:
    db2 = cantools.database.load_string(fin.read())

print(db2.version)
example_message2 = db2.get_message_by_name('ExampleMessage')
pprint(example_message2.signals)
