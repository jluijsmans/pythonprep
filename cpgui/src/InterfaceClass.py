import tkinter as tk
from tkinter import ttk
from tkinter import scrolledtext
from tkinter import Menu
from tkinter import messagebox as msg
import ToolTipClass as ToolTip
import time

class Interface():

    def __init__(self):
        self.win = tk.Tk()
        self.win.title("Python guike")
        # Radiobutton globals
        self.colors = ["Blue", "Gold", "Red"]
        self.create_widgets()

    # Callback function for the button
    def click_me(self):
        self.action.configure(text='Hello ' + self.name.get() + ' ' + self.number_chosen.get())

    # radiobutton callback
    def radCall(self):
        self.radSel=self.radVar.get()
        if self.radSel == 1: self.mighty2.config(text=self.colors[1])
        if self.radSel == 2: self.mighty2.config(text=self.colors[2])
        if self.radSel == 3: self.mighty2.configure(text=self.colors[3])

    def _quit(self):
        self.win.quit()
        self.win.destroy()
        exit()

    def _msgBox(self):
        msg.showinfo('Python message info box', 'A python GUI created using tkinter:\nThe year is 2019')

    def _exitBox(self):
        answer = msg.askyesno("Exit message", "Are you sure yuo want to exit the program?")
        if answer:
            self._quit()

    def run_progressbar(self):
        self.progress_bar["maximum"] = 100
        for i in range(101):
            time.sleep(0.05)
            self.progress_bar["value"] = i
            self.progress_bar.update()
        self.progress_bar["value"] = 0

    def start_progressbar(self):
        self.progress_bar.start()

    def stop_progressbar(self):
        self.progress_bar.stop()

    def progressbar_stop_after(self, wait_ms=1000):
        self.win.after(wait_ms, self.progress_bar.stop)

    def create_widgets(self):
        tabControl = ttk.Notebook(self.win)
        tab1 = ttk.Frame(tabControl)
        tab2 = ttk.Frame(tabControl)
        tabControl.add(tab1, text='Tab 1')
        tabControl.add(tab2, text='Tab 2')
        tabControl.pack(expand=1, fill="both")

        ###################################################################################################

        #Labelframe using tab1 as the parent
        mighty = ttk.LabelFrame(tab1, text=' Mighty Python ')
        mighty.grid(column=0, row=0, padx=8, pady=4)

        # Labelframe using tab2 as parent
        self.mighty2 = ttk.LabelFrame(tab2, text='The snake')
        self.mighty2.grid(column=0, row=0, padx=8, pady=4)

        # Label using mighty as the parent
        a_label = ttk.Label(mighty, text="Enter a name")
        a_label.grid(column=0, row=0, sticky='W')


        # Create button that executes click_me when clicked
        self.action = ttk.Button(mighty, text="Click me!", command=self.click_me)
        self.action.grid(column=2, row=1)

        # Adding text box widget
        self.name = tk.StringVar()
        name_entered = ttk.Entry(mighty, width=12, textvariable=self.name)
        name_entered.grid(column=0, row=1)

        # Adding a combobox
        ttk.Label(mighty, text="Choose a number:").grid(column=1, row=0)
        self.number = tk.StringVar()
        self.number_chosen = ttk.Combobox(mighty, width=12, textvariable=self.number)
        self.number_chosen['values'] = (1,2,4,6,8,2,9,31520)
        self.number_chosen.grid(column=1, row=1)
        self.number_chosen.current(0)

        # Creating 3 checkboxes in different init state
        self.chVarDis = tk.IntVar()
        check1 = tk.Checkbutton(self.mighty2, text='Disabled', variable=self.chVarDis, state='disabled')
        check1.select()
        check1.grid(column=0, row=4, sticky=tk.W)

        self.chVarUn = tk.IntVar()
        check2 = tk.Checkbutton(self.mighty2, text='UnChecked', variable=self.chVarUn)
        check2.deselect()
        check2.grid(column=1, row=4, sticky=tk.W)

        self.chVarEn = tk.IntVar()
        check3 = tk.Checkbutton(self.mighty2, text='Enabled', variable=self.chVarEn)
        check3.select()
        check3.grid(column=2, row=4, sticky=tk.W)
        ToolTip.create_tooltip(check3, 'the enabled checkbox')


        # Create three radiobuttons
        self.radVar = tk.IntVar()

        for col in range(3):
            self.curRad = tk.Radiobutton(self.mighty2, text=self.colors[col], variable=self.radVar, value=col, command=self.radCall)
            self.curRad.grid(column=col, row=5, sticky=tk.W)

        ##############################################################################################################

        # Create a scrolled text box
        scrol_w = 30
        scrol_h = 3
        scr = scrolledtext.ScrolledText(mighty, width=scrol_w, height=scrol_h, wrap=tk.WORD)
        scr.grid(column=0, row=6, sticky='WE', columnspan=3)

        # Add a tooltip to the ScrolledText widget
        ToolTip.create_tooltip(scr, 'This is a scrolledtext widget')

        # Creating a menu bar
        self.menu_bar = Menu(self.win)
        self.win.config(menu=self.menu_bar)

        # Create menu and add menu items
        file_menu = Menu(self.menu_bar, tearoff=0)
        file_menu.add_command(label="New")
        file_menu.add_separator()
        file_menu.add_command(label="Exit", command=self._exitBox)
        self.menu_bar.add_cascade(label="File", menu=file_menu)


        # Add another Menu to the menu bar and add items
        help_menu = Menu(self.menu_bar, tearoff=0)
        self.menu_bar.add_cascade(label="Help", menu=help_menu)
        help_menu.add_command(label="About", command=self._msgBox)

        #====================== Progressbar =============================================================

        # Create a container to hold labels
        buttons_frame = ttk.LabelFrame(tab2, text='  Progressbar buttons')
        buttons_frame.grid(column=0, row=5)

        ttk.Button(buttons_frame, text=" Run Progressbar ", command=self.run_progressbar).grid(column=0, row=0, sticky='W')
        ttk.Button(buttons_frame, text=" Start Progressbar ", command=self.start_progressbar).grid(column=0, row=1, sticky='W')
        ttk.Button(buttons_frame, text=" Stop immediately ", command=self.stop_progressbar).grid(column=0, row=2, sticky='W')
        ttk.Button(buttons_frame, text=" Stop after second ", command=self.progressbar_stop_after).grid(column=0, row=3, sticky='W')

        # Add a progressbar to tab2
        self.progress_bar = ttk.Progressbar(tab2, orient='horizontal', length=286, mode='determinate')
        self.progress_bar.grid(column=0, row=6, pady=2)

        # Set focus to the textfield, the cursor starts here now
        name_entered.focus()
